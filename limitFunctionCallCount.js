function limitFunctionCallCount(cb,n){
    return function(){
        if (n>0){
            cb();
            n--;
        }
        else{
            console.log("cb is already invoked");
        }
    }
}
export {limitFunctionCallCount}
