function cacheFunction(cb){
    let cache={}
    return function(val){
        if (val in cache){
            console.log("The below value returned from cache");
            return cache[val];

        }
        else{
            cache[val]=cb(val); 
            return cache[val];   

        }
        

    }
}
export {cacheFunction}
