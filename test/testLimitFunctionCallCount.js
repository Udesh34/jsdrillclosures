import {limitFunctionCallCount} from '../limitFunctionCallCount.js';
const invoke=limitFunctionCallCount(function cb(){
    console.log("cb is invoked");
},3);
invoke();
invoke();
invoke();
invoke();
invoke();

