import {cacheFunction} from '../cacheFunction.js';
const invokeFunction=cacheFunction(function cb(n){
    return n*10;
});
console.log(invokeFunction(2),'\n');
console.log(invokeFunction(3),'\n');
console.log(invokeFunction(2),'\n');
console.log(invokeFunction(4),'\n');
console.log(invokeFunction(3),'\n');
